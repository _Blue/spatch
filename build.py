#! /usr/bin/python3

import os
import json
import sys
import traceback
import subprocess

DEFAULT_INDEX = "index.html" # Default root document
OUTPUT_DIR = "" # Directory where apache configuration will be written
SITE_ROOT = "" # Root of the sub domain directory
CONFIG_FILE = "config.json" # Name of the configuration file
GET_SSL_CERT = True # Enable let's encrypt agent
EMAIL = "" # Give an email to let's encrypt agent


created = list()

def load():
    for path in os.listdir(SITE_ROOT):
        full_path = os.path.join(SITE_ROOT, path)
        if os.path.isdir(full_path) and path[0] != ".": # Avoid reading .git
            read_root(full_path)


def read_root(path: str):
    domains = []
    index = DEFAULT_INDEX
    default_root = False
    ssl_redirect = True

    config_file = os.path.join(path, CONFIG_FILE)
    print("reading: " + config_file)
    with open(config_file, 'r') as file:
        object = json.loads(file.read())

        for domain in object["domains"]:
            domains.append(domain)

        if "index" in object:
            index = object["index"]

        if "default" in object:
            default_root = object["default"]

        if "ssl_redirect" in object:
            ssl_redirect = object["ssl_redirect"]

    if default_root == (len(domains) != 0):
        raise Exception("Config cannot have domains and default_root = True")

    name = path.split("/")[-1]

    for domain in domains:
        if domain in created:
            print("WARNING: duplicated configuration for: " + domain + ", in: " + config_file)

    write_config(name, path, domains, default_root, index, ssl_redirect)
    created.extend(domains)

def write_config(filename: str, root_path: str, domains: list, default_root: bool, index: str, ssl_redirect: bool):
    file = open(os.path.join(OUTPUT_DIR, filename), "w")
    write_vhost(file, root_path, domains, default_root, index, False)
    write_vhost(file, root_path, domains, default_root, index, True)

    file.close()


def write_vhost(file, root_path: str, domains: list, default_root: bool, index: str, ssl: bool):
    port = 443 if ssl else 80

    file.write("<VirtualHost %s:%i>\n" % ("_default_" if default_root else "*", port))
    file.write("    DocumentRoot \"%s\"\n"
               "    DirectoryIndex %s\n"
               "    <Directory \"%s\">\n"
               "        Options FollowSymLinks\n"
               "        AllowOverride None\n"
               "        Require all granted\n"
               "    </Directory>\n" % (root_path, index, root_path))

    file.write("    <Files ~ \"%s$\">\n"
               "         Order allow,deny\n"
               "         Deny from all\n"
               "     </Files>\n" % CONFIG_FILE)

    if not default_root:
        file.write("  ServerName " + domains[0] + "\n")

    for domain in domains[1:]:
        file.write("  ServerAlias " + domain + "\n")

    if ssl:
        file.write("    SSLEngine on\n")

    file.write("</VirtualHost>\n")


def get_certificate():
    process = subprocess.Popen(["/usr/bin/certbot-auto",
                                "run",
                                "-n", # Non-interactive mode
                                "--expand", # Expand existing certificate
                                "--apache", # Apache mode
                                "--agree-tos", # Accept term of service
                                "--email", # Set user email
                                EMAIL,
                                "-d", # Specify domains
                                ",".join(created)])

    process.wait()

    if process.returncode != 0:
        raise Exception("Failed to obtain SSL certificate")

if __name__ == '__main__':
    try:
        load()
        get_certificate()
    except Exception as e:
        print(traceback.format_exc())
        sys.exit(1)

    print("Created configuration files for " + str(len(created)) + " domains")
